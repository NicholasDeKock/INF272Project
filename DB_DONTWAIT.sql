USE [master]
GO

CREATE DATABASE DONTWAIT

ALTER DATABASE DONTWAIT SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC DONTWAIT.[dbo].[sp_fulltext_database] @action = 'enable'
end
ALTER DATABASE DONTWAIT SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE DONTWAIT SET ANSI_NULLS OFF
GO
ALTER DATABASE DONTWAIT SET ANSI_PADDING OFF
GO
ALTER DATABASE DONTWAIT SET ANSI_WARNINGS OFF
GO
ALTER DATABASE DONTWAIT SET ARITHABORT OFF
GO
ALTER DATABASE DONTWAIT SET AUTO_CLOSE OFF
GO
ALTER DATABASE DONTWAIT SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE DONTWAIT SET AUTO_SHRINK OFF
GO
ALTER DATABASE DONTWAIT SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE DONTWAIT SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE DONTWAIT SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE DONTWAIT SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE DONTWAIT SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE DONTWAIT SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE DONTWAIT SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE DONTWAIT SET  DISABLE_BROKER
GO
ALTER DATABASE DONTWAIT SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE DONTWAIT SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE DONTWAIT SET TRUSTWORTHY OFF
GO
ALTER DATABASE DONTWAIT SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE DONTWAIT SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE DONTWAIT SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE DONTWAIT SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE DONTWAIT SET  READ_WRITE
GO
ALTER DATABASE DONTWAIT SET RECOVERY SIMPLE
GO
ALTER DATABASE DONTWAIT SET  MULTI_USER
GO
ALTER DATABASE DONTWAIT SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE DONTWAIT SET DB_CHAINING OFF
GO

USE DONTWAIT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE Title(
	TitleID int IDENTITY(1,1) NOT NULL,
	TitleDescription varchar (10) NULL,
 CONSTRAINT [PK_Title] PRIMARY KEY CLUSTERED 
(
	[TitleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/*** User ***/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE TheUser(
	UserID int IDENTITY(1,1) NOT NULL,
	UserSescription [varchar](50) NULL,
 CONSTRAINT [PK_TheUser] PRIMARY KEY CLUSTERED 
(
	UserID ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/*** Admin ****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE TheAdmin(
	AdminID int IDENTITY(1,1) NOT NULL,
	Name varchar(50) NULL,
	Surname varchar (50) NULL,
	EmailAddress varchar (50) NULL,
	IDNumber varchar(13) NULL,
	CellphoneNumber int,
	AdminPassword varchar(20) NULL,
	UserID int NOT NULL,
	TitleID int NOT NULL,
 CONSTRAINT [PK_TheAdmin] PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object ******/
ALTER TABLE TheAdmin  WITH CHECK ADD  CONSTRAINT [FK_Admin_User] FOREIGN KEY(UserID)
REFERENCES TheUser (UserID)
GO
ALTER TABLE TheAdmin CHECK CONSTRAINT [FK_Admin_User]
GO
/****** Object ******/
ALTER TABLE TheAdmin  WITH CHECK ADD  CONSTRAINT [FK_Admin_Title] FOREIGN KEY(TitleID)
REFERENCES Title (TitleID)
GO
ALTER TABLE TheAdmin CHECK CONSTRAINT [FK_Admin_Title]
GO
/****** Province  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE Province(
	ProvinceID int IDENTITY(1,1) NOT NULL,
	Name varchar(50) NULL,
 CONSTRAINT [PK_Province] PRIMARY KEY CLUSTERED 
(
	[ProvinceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** City  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE City(
	CityID int IDENTITY(1,1) NOT NULL,
	Name varchar(50) NULL,
	ProvinceID int NOT NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[CityID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object ******/
ALTER TABLE City  WITH CHECK ADD  CONSTRAINT [FK_City_Province] FOREIGN KEY(ProvinceID)
REFERENCES Province (ProvinceID)
GO
ALTER TABLE City CHECK CONSTRAINT [FK_City_Province]
GO
/****** Suburb  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE Suburb(
	SuburbID int IDENTITY(1,1) NOT NULL,
	Name varchar(50) NULL,
	CityID int NOT NULL,
 CONSTRAINT [PK_Suburb] PRIMARY KEY CLUSTERED 
(
	[SuburbID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object ******/
ALTER TABLE Suburb  WITH CHECK ADD  CONSTRAINT [FK_Suburb_City] FOREIGN KEY(CityID)
REFERENCES City (CityID)
GO
ALTER TABLE Suburb CHECK CONSTRAINT [FK_Suburb_City]
GO
/****** Clinic  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE Clinic(
	ClinicID int IDENTITY(1,1) NOT NULL,
	Name varchar(50) NULL,
	EmailAddress varchar (50) NULL,
	TelephoneNumber int,
	SuburbID int NOT NULL,
 CONSTRAINT [PK_Clinic] PRIMARY KEY CLUSTERED 
(
	[ClinicID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object ******/
ALTER TABLE Clinic  WITH CHECK ADD  CONSTRAINT [FK_Clinic_Suburb] FOREIGN KEY(SuburbID)
REFERENCES Suburb (SuburbID)
GO
ALTER TABLE Clinic CHECK CONSTRAINT [FK_Clinic_Suburb]
GO
/****** MedicalAid  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE MedicalAid(
	MedicalAidID int IDENTITY(1,1) NOT NULL,
	Name varchar(50) NULL,
	MedicalAidContact int NULL,
 CONSTRAINT [PK_MedicalAid] PRIMARY KEY CLUSTERED 
(
	[MedicalAidID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Parent  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE Parent(
	ParentID int IDENTITY(1,1) NOT NULL,
	Name varchar(50) NULL,
	Surname varchar (50) NULL,
	EmailAddress varchar (50) NULL,
	IDNumber varchar(13) NULL,
	CellphoneNumber int,
	ParentPassword varchar(20) NULL,
	ClinicID int NOT NULL,
	MedicalAidID int NOT NULL,
	TitleID int NOT NULL,
	UserID int NOT NULL, 
 CONSTRAINT [PK_Parent] PRIMARY KEY CLUSTERED 
(
	[ParentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object ******/
ALTER TABLE Parent  WITH CHECK ADD  CONSTRAINT [FK_Parent_Clinic] FOREIGN KEY(ClinicID)
REFERENCES Clinic (ClinicID)
GO
ALTER TABLE Parent CHECK CONSTRAINT [FK_Parent_Clinic]
GO
/****** Object ******/
ALTER TABLE Parent  WITH CHECK ADD  CONSTRAINT [FK_Parent_Title] FOREIGN KEY(TitleID)
REFERENCES Title (TitleID)
GO
ALTER TABLE Parent CHECK CONSTRAINT [FK_Parent_Title]
GO
/****** Object ******/
ALTER TABLE Parent  WITH CHECK ADD  CONSTRAINT [FK_Parent_MedicalAid] FOREIGN KEY(MedicalAidID)
REFERENCES MedicalAid (MedicalAidID)
GO
ALTER TABLE Parent CHECK CONSTRAINT [FK_Parent_MedicalAid]
GO
/****** Object ******/
ALTER TABLE Parent  WITH CHECK ADD  CONSTRAINT [FK_Parent_User] FOREIGN KEY(UserID)
REFERENCES TheUser (UserID)
GO
ALTER TABLE Parent CHECK CONSTRAINT [FK_Parent_User]
GO
/*** Child ****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE Child(
	ChildID int IDENTITY(1,1) NOT NULL,
	Name varchar(50) NULL,
	Surname varchar (50) NULL,
	Age int NULL,
	IDNumber varchar(13) NULL,
 CONSTRAINT [PK_Child] PRIMARY KEY CLUSTERED 
(
	[ChildID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table ParentChild ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE ParentChild(
	ParentChildID int IDENTITY (1,1) NOT NULL,
	ParentID int NOT NULL,
	ChildID int NOT NULL
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_RecipeIngredient_Ingredient]    Script Date: 08/05/2019 23:16:24 ******/
ALTER TABLE ParentChild  WITH CHECK ADD  CONSTRAINT [FK_ParentChild_Child] FOREIGN KEY(ChildID)
REFERENCES Child (ChildID)
GO
ALTER TABLE ParentChild CHECK CONSTRAINT [FK_ParentChild_Child]
GO
/****** Object:  ForeignKey [FK_RecipeIngredient_Recipe]    Script Date: 08/05/2019 23:16:24 ******/
ALTER TABLE ParentChild  WITH CHECK ADD  CONSTRAINT [FK_ParentChild_Parent] FOREIGN KEY(ParentID)
REFERENCES Parent ([ParentID])
GO
ALTER TABLE ParentChild CHECK CONSTRAINT [FK_ParentChild_Parent]
GO
/****** Booking  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE Booking(
	BookingID int IDENTITY(1,1) NOT NULL,
	BDescription varchar (50) NULL,
	BDate Date NULL,
	ParentID int NOT NULL,
	ClinicID int NOT NULL,
	ChildID int NOT NULL,
 CONSTRAINT [PK_Booking] PRIMARY KEY CLUSTERED 
(
	BookingID ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object: ******/
ALTER TABLE Booking  WITH CHECK ADD  CONSTRAINT [FK_Booking_Parent] FOREIGN KEY(ParentID)
REFERENCES Parent (ParentID)
GO
ALTER TABLE Booking CHECK CONSTRAINT [FK_Booking_Parent]
GO
/****** Object: ******/
ALTER TABLE Booking  WITH CHECK ADD  CONSTRAINT [FK_Booking_Clinic] FOREIGN KEY(ClinicID)
REFERENCES Clinic (ClinicID)
GO
ALTER TABLE Booking CHECK CONSTRAINT [FK_Booking_Clinic]
GO
/****** Object: ******/
ALTER TABLE Booking  WITH CHECK ADD  CONSTRAINT [FK_Booking_Child] FOREIGN KEY(ChildID)
REFERENCES Child (ChildID)
GO
ALTER TABLE Booking CHECK CONSTRAINT [FK_Booking_Child]
GO
/****** Object:  Table Vaccination ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE Vaccination(
	VaccinationID int IDENTITY(1,1) NOT NULL,
	Name varchar(50) NULL,
	Age int NULL,
 CONSTRAINT [PK_Vaccination] PRIMARY KEY CLUSTERED 
(
	[VaccinationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table ChildVaccination ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE ChildVaccination(
	ChildVaccinationID int IDENTITY (1,1) NOT NULL,
	ChildID int NOT NULL,
	VaccinationID int NOT NULL
) ON [PRIMARY]
GO
/****** Object:  ******/
ALTER TABLE ChildVaccination  WITH CHECK ADD  CONSTRAINT [FK_ChildVaccination_Vaccination] FOREIGN KEY(VaccinationID)
REFERENCES Vaccination (VaccinationID)
GO
ALTER TABLE ChildVaccination CHECK CONSTRAINT [FK_ChildVaccination_Vaccination]
GO
/****** Object ******/
ALTER TABLE ChildVaccination  WITH CHECK ADD  CONSTRAINT [FK_ChildVaccination_Child] FOREIGN KEY(ChildID)
REFERENCES Child (ChildID)
GO
ALTER TABLE ChildVaccination CHECK CONSTRAINT FK_ChildVaccination_Child
GO
/****** Object:  Table SideEffects ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE SideEffect(
	SideEffectID int IDENTITY(1,1) NOT NULL,
	Name varchar(50) NULL,
 CONSTRAINT [PK_SideEffect] PRIMARY KEY CLUSTERED 
(
	[SideEffectID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table ChildVaccination ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE VaccinationSideEffect(
	VaccinationSideEffectID int IDENTITY (1,1) NOT NULL,
	VaccinationID int NOT NULL,
	SideEffectID int NOT NULL,
) ON [PRIMARY]
GO
/****** Object: ******/
ALTER TABLE VaccinationSideEffect  WITH CHECK ADD  CONSTRAINT [FK_VaccinationSideEffect_SideEffect] FOREIGN KEY(SideEffectID)
REFERENCES SideEffect (SideEffectID)
GO
ALTER TABLE ChildVaccination CHECK CONSTRAINT FK_ChildVaccination_Child
GO
/****** Object ******/
ALTER TABLE VaccinationSideEffect  WITH CHECK ADD  CONSTRAINT [FK_VaccinationSideEffect_Vaccination] FOREIGN KEY(VaccinationID)
REFERENCES Vaccination (VaccinationID)
GO
ALTER TABLE VaccinationSideEffect CHECK CONSTRAINT [FK_VaccinationSideEffect_Vaccination]
GO