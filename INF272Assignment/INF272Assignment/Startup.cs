﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(INF272Assignment.Startup))]
namespace INF272Assignment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
