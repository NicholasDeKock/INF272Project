//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TheAdmin
    {
        public int AdminID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string IDNumber { get; set; }
        public Nullable<int> CellphoneNumber { get; set; }
        public string AdminPassword { get; set; }
        public int UserID { get; set; }
        public int TitleID { get; set; }
    
        public virtual Title Title { get; set; }
        public virtual TheUser TheUser { get; set; }
    }
}
